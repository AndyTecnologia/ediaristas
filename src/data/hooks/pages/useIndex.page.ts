import { useState, useMemo } from 'react';
import { UserShortInterface} from 'data/@types/userInterface';
import {ValidationService } from 'data/services/validationService';
import { ApiService } from 'data/services/apiService';

export default function useIndex(){
    const [cep, setCep] = useState(''),
        cepValido = useMemo(() => {
            return ValidationService.cep(cep);
        },[cep]),
        [erro, setErro] = useState(''),
        [buscaFeita, setbuscaFeita] = useState(false),
        [carregando, setcarregando] = useState(false),
        [diaristas, setdiaristas] = useState([] as UserShortInterface[]),
        [diaristasRestantes, setdiaristasRestantes] = useState(0);

    async function buscarProfissionais(cep: string){
        setbuscaFeita(false);
        setcarregando(true);
        setErro('');
        try{
            const {data} = await ApiService.get<{
                diaristas: UserShortInterface[],
                quantidade_diaristas: number
            }>('/api/diaristas-cidade?cep='+cep.replace(/\D/g,''))

            setdiaristas(data.diaristas);
            setdiaristasRestantes(data.quantidade_diaristas);
            setbuscaFeita(true);
            setcarregando(false);
        }catch(error){
            setErro('CEP não encontrado');
            setcarregando(false);
        }   

    }

    return {
        cep,
        setCep,
        cepValido,
        buscarProfissionais,
        erro,
        diaristas,
        buscaFeita,
        carregando,
        diaristasRestantes
    };

};