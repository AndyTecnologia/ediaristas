import React from 'react';
import { FooterStyled,FooterContainer,FooterTitle,AppList } from './Footer.style';
import {Typography, Box} from '@mui/material';

const Footer = () => {
    return (<FooterStyled>
            <FooterContainer>
                <Box sx={{ maxWidth:'400px'}}>
                <FooterTitle >Quem Somos</FooterTitle>
                <Typography variant={'body2'} sx={{marginTop: 2}}> Lorem ipsum dolor sit amet consectetur, 
                    adipisicing elit. Officiis nobis sit dolor dolorem 
                    obcaecati unde earum incidunt! Autem blanditiis unde quibusdam, 
                    sint voluptatibus esse earum molestias omnis. Corrupti, aliquam doloremque.
                </Typography>
                </Box>

                <div>
                <FooterTitle >Baixe nossoas aplicativos</FooterTitle> 
                <AppList>
                    <li>
                        <a
                            href={'/'}
                            target={'_blank'}
                            rel={'noopener noreferrer'}
                        >
                            <img  src={'/img/logos/app-store.png'}
                            alt={'App Store'} 
                            />
                        </a>
                    </li>
                    <li>
                        <a
                            href={'/'}
                            target={'_blank'}
                            rel={'noopener noreferrer'}
                        >
                            <img  src={'/img/logos/google-play.png'}
                            alt={'Google Play'} 
                            />
                        </a>
                    </li>
                </AppList>
                </div>
            </FooterContainer>
    </FooterStyled>);
}

export default Footer;