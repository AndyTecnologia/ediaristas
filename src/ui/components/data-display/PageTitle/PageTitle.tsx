import React from 'react';
import { PageTitleStyled, PageSubitleStyled, PageTitleContainer } from './PageTitle.style';


interface PageTitleProps{
    title: string;
    subtitle: string;
}

const PageTitle: React.FC <PageTitleProps> = (props) => {
    return (
    <PageTitleContainer>
        <PageTitleStyled>
            {props.title}
        </PageTitleStyled>
        <PageSubitleStyled>
            {props.subtitle}
            </PageSubitleStyled>
    </PageTitleContainer>
    );
}

export default PageTitle;